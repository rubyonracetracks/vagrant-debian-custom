# Vagrant Debian Customizer

## What's the point?
This repository configures a Vagrant environment for setting up a virtual development environment when you join a legacy project.

## How does this repository work?
* Running the main.sh asks you for the desired name of your new Vagrant repository, downloads the source code of the [Vagrant Debian General repository](https://gitlab.com/rubyonracetracks/vagrant-debian-general/), and then customizes the new repository with the new name you requested.  Bash scripts are provided in your custom Vagrant repository to start up, reboot, and rebuild your Vagrant box.
* You can make further customizations to your new repository.

## Usage
* Download the source code of this repository.
* In your terminal, use the "cd" command to enter the root directory of this app.
* Enter the command "bash main.sh". This starts this app.
* When prompted, enter the directory name and title.
* The [Vagrant Debian General repository](https://gitlab.com/rubyonracetracks/vagrant-debian-general) will be downloaded into the "new" directory, and it will be customized with the new directory name and title.
* Follow the instructions for starting a new Git repository for your new Vagrant setup.
* Push your new Git repository to GitLab, GitHub, or BitBucket.
* From the root directory of your new Vagrant repository, enter the command `bash login.sh` to download, provision, and ssh into the Vagrant box.
* Use the shared directory as the working directory.  The /home/vagrant/shared directory in the Vagrant environment and the shared directory within your new Vagrant repository are synced together.
* If necessary, modify your Vagrant setup for the new legacy project.

## Why use Vagrant when Docker is so much faster and more efficient?
* Some projects are difficult to Dockerize.  When you're starting a new project that's difficult to Dockerize, having to learn how to set it up AND having to figure out how to Dockerize it is too much to do all at once.
* When the setup instructions assume that you're relying on the host system for your development environment, it's better to start off by relying on a Vagrant environment (which provides an experience much more similar to that of the host environment than that of the Docker environment) and following the instructions provided.  If weird things happen, or if you want to make sure that the setup procedure is complete, relying on the Vagrant environment instead of the host environment means that you can reset everything and be back in business in a matter of minutes instead of hours.
* Yes, Vagrant is slower and cruder than Docker.  However, it's better to have a slower and cruder way that works than a faster and more elegant way that falls short.  Once you get the project working in the Docker environment, you'll know the project much better and be in a much stronger position to Dockerize it.
* In other words, you should grab half a loaf immediately.  Once you have the first half, you still have the option of going for the second half.  You'll also be in a much stronger position to get it.

## Included Features
See the [Vagrant Debian General repository](https://gitlab.com/rubyonracetracks/vagrant-debian-general) for the details.

## Why do the Vagrant box provisioning scripts include things I don't need in the project?
It's better to have a feature and not need it than to omit something that's necessary.  But if you don't need that tool for your project, you can edit the Vagrant box provisioning scripts to omit it and speed up the provisioning process.

## Why don't you publish boxes in Vagrant Cloud anymore?
* Publishing boxes requires uploading a very large file.  My home Internet connection has an extremely slow upload speed, and it would take hours to complete the upload.  In contrast, the local provisioning process takes minutes.
* It is impossible to build a Vagrant box in Travis CI or any other continuous integration environment.  Virtual machines do NOT work inside the Docker environment.
* It is impossible to build a 64-bit virtual machine within another 64-bit virtual machine.  So relying on Google Cloud Computing, AWS, or Azure is not an option.
