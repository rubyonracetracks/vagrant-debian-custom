#!/bin/bash
set -e

DIR_NAME=$1
TITLE=$2

mkdir -p $PWD/new

DIR_APP=$PWD/new/$DIR_NAME

git clone https://gitlab.com/rubyonracetracks/vagrant-debian-general.git $DIR_APP

wait

# Necessary to avoid accidental commits to the wrong repository
cd $DIR_APP && git remote remove origin

# Update the README.md file
rm $DIR_APP/README.md
mv $DIR_APP/README-new.md $DIR_APP/README.md
wait

# Update the title of the app
STR1='Vagrant Debian - General'
STR2=$TITLE
find $DIR_APP -type f -exec sed -i "s|$STR1|$STR2|g" {} +

# Update the directory name and virtual machine name
STR1='vagrant-debian-general'
STR2="$DIR_NAME"
find $DIR_APP -type f -exec sed -i "s|$STR1|$STR2|g" {} +

echo '#############################################'
echo 'Your new Vagrant repository has been created!'
echo ''
echo 'It is located at:'
echo "$DIR_APP"
echo ''
echo 'The README.md file contains instructions on what to do next.'
