#!/bin/bash

echo '*****************************************'
echo 'Welcome to the Vagrant Debian Customizer!'
echo ''
echo 'Enter the name of the directory you wish to use for your new repository.'
echo 'If you enter a blank, a directory name will be chosen for you.'
read DIR_NAME

if [ -z "$DIR_NAME" ]; then
  DATE=`date +%Y%m%d-%H%M%S-%3N`
  DIR_NAME="vagrant-debian-$DATE"
fi

echo ''
echo 'Enter the title of your new repository.'
echo 'If you enter a blank, an app title will be chosen for you.'
read TITLE

if [ -z "$TITLE" ]; then
  TITLE='Vagrant Debian - Blah Blah Blah'
fi

echo '--------------------------'
echo 'Repository directory name:'
echo "$DIR_NAME"
echo ''
echo '-----------------'
echo 'Repository title:'
echo "$TITLE"
echo ''

echo ''
echo 'Press Enter to continue.'
read CONT

mkdir -p log
bash exec-main.sh "$DIR_NAME" "$TITLE"
